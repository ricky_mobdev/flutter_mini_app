import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:find_dropdown/find_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:kawal_covid/models/regions_model.dart';
import 'package:kawal_covid/providers/news_provider.dart';
import 'package:kawal_covid/providers/statistic_provider.dart';
import 'package:kawal_covid/widgets/card.dart';
import 'package:kawal_covid/widgets/card_news.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with WidgetsBindingObserver {
  String selectedCountry = 'Indonesia';
  List countryList; 
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    Future.microtask(() async {
      Provider.of<StatisticProvider>(context, listen: false).fetchRegion();
      Provider.of<NewsProvider>(context, listen: false).fetchNews();
      Provider.of<StatisticProvider>(context, listen: false)
          .fetchReportByCountry("Indonesia");
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.paused) {
      print('running background');
    }
    if (state == AppLifecycleState.resumed) {
      print('running foreground');
    }
  }

  Widget dropDownCountry(BuildContext context) {
    var regional = Provider.of<StatisticProvider>(context).regions;
    countryList = Provider.of<StatisticProvider>(context, listen: false).countryList;
      return FindDropdown(
        selectedItem: selectedCountry,
        items: countryList,
        showSearchBox: true,
        labelStyle: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
        searchBoxDecoration: InputDecoration(
            labelText: "Pilih Negara", prefixIcon: Icon(EvaIcons.search)),
        onChanged: (text) {
          setState(() {
            selectedCountry = text;
          });
          Provider.of<StatisticProvider>(context, listen: false)
              .fetchReportByCountry(text);
        },
      );
    } 
  

  Widget cardStatistic(BuildContext context) {
    var statistic = Provider.of<StatisticProvider>(context).statistic;
    return GridView.count(
      childAspectRatio: 1.4,
      crossAxisCount: 2,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      children: <Widget>[
        Container(
            margin: EdgeInsets.only(top: 5),
            child: statistic != null
                ? BaseCard("Cases", statistic.data[0].confirmed.toString())
                : Container()),
        Container(
            margin: EdgeInsets.only(top: 5),
            child: statistic != null
                ? BaseCard("Deaths", statistic.data[0].deaths.toString())
                : Container()),
        Container(
            margin: EdgeInsets.only(top: 5),
            child: statistic != null
                ? BaseCard("Recovered", statistic.data[0].recovered.toString())
                : Container()),
        Container(
            margin: EdgeInsets.only(top: 5),
            child: statistic != null
                ? BaseCard("Active", statistic.data[0].active.toString())
                : Container()),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var loading = Provider.of<StatisticProvider>(context).isloading;
    var news = Provider.of<NewsProvider>(context).news;
    return Scaffold(
        appBar: AppBar(
          title: Text("Mini App"),
          backgroundColor: Color(0xff141518),
        ),
        backgroundColor: Color(0xff292929),
        body: SingleChildScrollView(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                dropDownCountry(context),
                SizedBox(
                  height: 30.0,
                  
                ),
                cardStatistic(context),
                news != null
                    ? Text(
                        "Hot News",
                        style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      )
                    : Container(),
                SizedBox(
                  height: 10,
                ),
                news != null
                    ? ListView(
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        physics: const NeverScrollableScrollPhysics(),
                        children: news.articles.map<Widget>((news) {
                          return NewsCard(context, news.title, news.content,
                              news.urlToImage, news.publishedAt);
                        }).toList())
                    : Container(),
              ],
            )));
  }
}
