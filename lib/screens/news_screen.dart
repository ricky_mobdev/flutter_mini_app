import 'package:flutter/material.dart';

class NewsScreen extends StatelessWidget {
  final title;
  final image;
  final content;

  const NewsScreen({Key key, @required this.title, this.image, this.content})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("News"),
        backgroundColor: Color(0xff141518),
      ),
      backgroundColor: Color(0xff292929),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              title,
              style: (TextStyle(
                color: Colors.white,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              )),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              height: 180,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  image: DecorationImage(
                      image: NetworkImage(image), fit: BoxFit.cover)),
            ),
            SizedBox(height: 30),
            content != null
                ? Text(
                    content,
                    style: (TextStyle(
                      color: Colors.white,
                      fontSize: 14.0,
                      letterSpacing: 0.4,
                      height: 1.4,
                    )),
                  )
                : Text(""),
          ],
        ),
      ),
    );
  }
}
