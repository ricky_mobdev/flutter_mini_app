import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:kawal_covid/models/news_model.dart';
import 'package:flutter/foundation.dart';

class NewsProvider with ChangeNotifier {
  News news;
  News get listNews => news;

  set listNews(News  news) {
    news = news;
    notifyListeners();
  }

  String errorMessage;
  bool isloading = false;

  fetchNews() async {
    setLoading(true);
    String url = 'https://newsapi.org/v2/top-headlines?country=id&pageSize=5&q=covid&apiKey=325fecb459f24c18a50e22d19b22fd15';
    Map<String, String> headers = {
      "Content-type": "application/json",
    };
    final response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      setLoading(false);
      var arr = json.decode(response.body);
      news = News.fromJson(arr);
      setNews(news);
    }
  }



  setNews(news) {
    news = news;
    notifyListeners();
  }

  setLoading(value) {
    isloading = value;
    notifyListeners();
  }

  bool isFetching() {
    return isloading;
  }
}
