import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:kawal_covid/models/regions_model.dart';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:kawal_covid/models/statistic_model.dart';

class StatisticProvider with ChangeNotifier {
  Regions regions;
  Regions get listRegions => regions;

  List countryList;

  set listRegions(Regions regions) {
    regions = regions;
    notifyListeners();
  }

  Statistic statistic;
  Statistic get dataStatistic => statistic;

  set dataStatistic(Statistic statistic) {
    statistic = statistic;
    notifyListeners();
  }

  String errorMessage;
  bool isloading = false;

  fetchRegion() async {
    setLoading(true);
    String url = 'https://covid-19-statistics.p.rapidapi.com/regions';
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-rapidapi-host": "covid-19-statistics.p.rapidapi.com",
      "x-rapidapi-key": "0b10130b30mshac9af4028af3356p1c4bd1jsn2374ca287d57"
    };
    final response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      setLoading(false);
      var arr = json.decode(response.body);
      regions = Regions.fromJson(arr);
      var tmpList = [];
      for (int i = 0; i < regions.data.length; i++) {
        tmpList.add(regions.data[i].name);
      }
      countryList = tmpList;
      setRegions(regions);
    }
  }

  fetchReportByCountry(text) async {
    setLoading(true);
    DateTime now = DateTime.now();
    var formattedDate = DateFormat('yyyy-MM-dd').format(now);
    String url =
        'https://covid-19-statistics.p.rapidapi.com/reports?date=2020-04-15&region_name=${text}';
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-rapidapi-host": "covid-19-statistics.p.rapidapi.com",
      "x-rapidapi-key": "0b10130b30mshac9af4028af3356p1c4bd1jsn2374ca287d57"
    };
    final response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      setLoading(false);
      var arr = json.decode(response.body);
      statistic = Statistic.fromJson(arr);
      setStatistic(regions);
    } else {
      print('error');
    }
  }

  setRegions(regions) {
    regions = regions;
    notifyListeners();
  }

  setStatistic(statistic) {
    statistic = statistic;
    notifyListeners();
  }

  setLoading(value) {
    isloading = value;
    notifyListeners();
  }

  bool isFetching() {
    return isloading;
  }
}
