class Statistic {
  List<Data> data;

  Statistic({this.data});

  Statistic.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String date;
  int confirmed;
  int deaths;
  int recovered;
  int confirmedDiff;
  int deathsDiff;
  int recoveredDiff;
  String lastUpdate;
  int active;
  int activeDiff;
  Region region;

  Data(
      {this.date,
      this.confirmed,
      this.deaths,
      this.recovered,
      this.confirmedDiff,
      this.deathsDiff,
      this.recoveredDiff,
      this.lastUpdate,
      this.active,
      this.activeDiff,
      this.region});

  Data.fromJson(Map<String, dynamic> json) {
    date = json['date'];
    confirmed = json['confirmed'];
    deaths = json['deaths'];
    recovered = json['recovered'];
    confirmedDiff = json['confirmed_diff'];
    deathsDiff = json['deaths_diff'];
    recoveredDiff = json['recovered_diff'];
    lastUpdate = json['last_update'];
    active = json['active'];
    activeDiff = json['active_diff'];
    region =
        json['region'] != null ? new Region.fromJson(json['region']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date;
    data['confirmed'] = this.confirmed;
    data['deaths'] = this.deaths;
    data['recovered'] = this.recovered;
    data['confirmed_diff'] = this.confirmedDiff;
    data['deaths_diff'] = this.deathsDiff;
    data['recovered_diff'] = this.recoveredDiff;
    data['last_update'] = this.lastUpdate;
    data['active'] = this.active;
    data['active_diff'] = this.activeDiff;
    if (this.region != null) {
      data['region'] = this.region.toJson();
    }
    return data;
  }
}

class Region {
  String iso;
  String name;
  String province;
  String lat;
  String long;

  Region({this.iso, this.name, this.province, this.lat, this.long});

  Region.fromJson(Map<String, dynamic> json) {
    iso = json['iso'];
    name = json['name'];
    province = json['province'];
    lat = json['lat'];
    long = json['long'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['iso'] = this.iso;
    data['name'] = this.name;
    data['province'] = this.province;
    data['lat'] = this.lat;
    data['long'] = this.long;

    return data;
  }
}
